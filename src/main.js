import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Axios from 'axios'

Vue.config.productionTip = false

new Vue({
  Axios,
  router,
  store,
  render: h => h(App)
}).$mount('#app')

/**Materialize CSS */
import 'materialize-css/dist/css/materialize.css'
import 'materialize-css/dist/js/materialize.js'